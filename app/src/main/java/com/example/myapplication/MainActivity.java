package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        InitItemList();
    }

    private void InitItemList() {
        List<Item> items = ItemListDataSource.getInstance().getItems();
        for (int i = 1; i <= 1000; i++) {
            items.add(new Item(R.drawable.baseline_supervisor_account_24,
                    getString(R.string.item_title) + " " + i,
                    getString(R.string.item_description) + " " + i));
        }
    }
}