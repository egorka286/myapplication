package com.example.myapplication;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;

import java.util.UUID;

public class DetailFragment extends Fragment {
    private Item mItem;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UUID itemId = (UUID) getArguments().getSerializable("itemId");
        mItem = ItemListDataSource.getInstance().getItem(itemId);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.fragment_detail, null);
        ((AppCompatImageView) view.findViewById(R.id.image)).setImageResource(mItem.getImageRes());
        ((TextView) view.findViewById(R.id.title)).setText(mItem.getTitle());
        ((TextView) view.findViewById(R.id.description)).setText(mItem.getDescription());
        return view;
    }

    public static DetailFragment newInstance(UUID itemId) {
        Bundle args = new Bundle();
        args.putSerializable("itemId", itemId);
        DetailFragment fragment = new DetailFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
