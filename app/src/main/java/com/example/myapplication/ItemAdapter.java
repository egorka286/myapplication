package com.example.myapplication;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.UUID;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder> {
    private final List<Item> mItems;
    private OnChangeItemListener mListener;

    public ItemAdapter(List<Item> items) {
        mItems = items;
    }

    public void addListener(OnChangeItemListener listener) {
        mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new ViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Item item = mItems.get(position);
        holder.bind(item);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private Item mItem;
        private final AppCompatImageView mImageView;
        private final TextView mTitleView;
        private final TextView mDescriptionView;
        private final OnChangeItemListener mListener;

        public void bind(Item item) {
            mItem = item;
            mImageView.setImageResource(mItem.getImageRes());
            mTitleView.setText(mItem.getTitle());
            mDescriptionView.setText(mItem.getDescription());
        }

        public ViewHolder(View itemView, OnChangeItemListener listener) {
            super(itemView);
            mListener = listener;
            mImageView = (AppCompatImageView) itemView.findViewById(R.id.image);
            mTitleView = (TextView) itemView.findViewById(R.id.title);
            mDescriptionView = (TextView) itemView.findViewById(R.id.description);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mListener != null) mListener.onChangeItem(mItem.getId());
        }
    }

    public interface OnChangeItemListener {
        void onChangeItem(UUID id);
    }
}
