package com.example.myapplication;

import java.util.UUID;

public class Item {
    private final UUID mId;
    private int mImageRes;
    private String mTitle;
    private String mDescription;

    public Item(int imageRes, String title, String description) {
        mId = UUID.randomUUID();
        setImageRes(imageRes);
        setTitle(title);
        setDescription(description);
    }

    public UUID getId() {
        return mId;
    }

    public int getImageRes() {
        return mImageRes;
    }

    public void setImageRes(int imageRes) {
        this.mImageRes = imageRes;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }
}
