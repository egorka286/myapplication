package com.example.myapplication;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ItemListDataSource {
    private static ItemListDataSource sItemList;
    private final List<Item> mItems;

    public static ItemListDataSource getInstance() {
        if (sItemList == null) {
            sItemList = new ItemListDataSource();
        }
        return sItemList;
    }

    public List<Item> getItems() {
        return mItems;
    }

    public Item getItem(UUID id) {
        for (Item item : mItems) {
            if (item.getId().equals(id)) {
                return item;
            }
        }
        return null;
    }

    private ItemListDataSource() {
        mItems = new ArrayList<>();
    }
}
